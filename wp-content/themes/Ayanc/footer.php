<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
$socials = get_social_links(['youtube', 'instagram', 'facebook', 'whatsapp' ]);
?>
<footer>
	<?php if ($current_id !== $contact_id) : ?>
		<div class="foo-form-block">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="form-wrapper">
							<div class="form-wrapper-inside">
								<?php if ($f_title = opt('foo_form_title')) : ?>
									<h2 class="form-title"><?= $f_title; ?></h2>
								<?php endif; ?>
								<div class="row justify-content-center align-items-center mb-4">
									<div class="col-auto">
										<?php if ($f_subtitle = opt('foo_form_subtitle')) : ?>
											<p class="form-subtitle mb-0"><?= $f_subtitle; ?></p>
										<?php endif; ?>
									</div>
									<div class="col-auto">
										<?php if ($f_text = opt('foo_form_text')) : ?>
											<p class="form-text mb-0"><?= $f_text; ?></p>
										<?php endif; ?>
									</div>
								</div>
								<?php getForm('10'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php else: if ($map = opt('map_image')) : ?>
		<a class="map-image w-100" href="<?= $map['url']; ?>" data-lightbox="map" style="background-image: url('<?= $map['url']; ?>')">
		</a>
	<?php endif;
	endif; ?>
	<div class="footer-main">
		<a id="go-top">
				<span class="pop-trigger-wrap">
					<span class="pop-trigger-inside">
						<img src="<?= ICONS ?>to-top.png">
					</span>
				</span>
			<h4 class="to-top-text mt-2">חזרה למעלה</h4>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-between align-items-start">
				<div class="col-xl-3 col-lg-auto col-sm-6 col-12 foo-menu">
					<h3 class="foo-title">
						תפריט ניווט
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu foo-links-menu">
					<h3 class="foo-title">
						<?php $foo_l_title = opt('foo_menu_title');
						echo $foo_l_title ? $foo_l_title : 'קישורים'; ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-sm-6 col-12 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						נהיה בקשר
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<?= svg_simple(ICONS.'foo-tel.svg'); ?><?= $tel; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= svg_simple(ICONS.'foo-mail.svg'); ?><?= $mail; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= svg_simple(ICONS.'foo-geo.svg'); ?><?= $address; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<?php if ($socials): ?>
							<div class="d-flex flex-wrap justify-content-start align-items-center mt-3">
								<?php foreach ($socials as $link) : ?>
									<a href="<?= $link['url']; ?>" class="social-link">
										<i class="fab fa-<?= ($link['name'] === 'facebook') ? $link['name'].'-f' : $link['name']; ?>"></i>
									</a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
