<?php
/*
Template Name: תהליך
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body process-page-back">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="base-output text-center">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['process_slider']) : ?>
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 arrows-slider">
				<div class="number-slider" dir="rtl">
					<?php foreach ($fields['process_slider'] as $slide) : ?>
						<div class="form-wrapper">
							<div class="row align-items-stretch">
								<div class="<?= $slide['process_img'] ? 'col-lg-6 col-12' : 'col-12'; ?> d-flex align-items-center">
									<div class="base-output process-slide-output">
										<?= $slide['process_text']; ?>
									</div>
								</div>
								<?php if ($slide['process_img']) : ?>
									<div class="col-lg-6 col-12 d-flex justify-content-center align-items-center">
										<img src="<?= $slide['process_img']['url']; ?>" class="slide-process-img" alt="slide-process-img">
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-12">
				<?php get_template_part('views/partials/repeat', 'form', [
						'title' => $fields['process_form_title'],
						'subtitle' => $fields['process_form_subtitle'],
						'text' => $fields['process_form_text'],
				]); ?>
			</div>
		</div>
	</div>
</article>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
			]);
}
get_footer(); ?>
