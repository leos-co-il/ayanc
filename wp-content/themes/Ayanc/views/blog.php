<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['posts_type'] ?? 'post';
$type_title = ($type === 'service') ? 'שירותים' : 'מאמרים';
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => $type,
]);
$published_posts = 0;
$count_posts = wp_count_posts($type);
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
?>

<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $i => $post) : ?>
							<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
							]); ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts && $published_posts > 6) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="<?= $type ?? 'post'; ?>">
						<?= 'טען עוד '.$type_title; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="form-and-benefits p-100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-12">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits'); ?>
</section>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
}
get_footer(); ?>
