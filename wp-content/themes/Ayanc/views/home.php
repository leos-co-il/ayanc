<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main"
		<?php if (has_post_thumbnail()) : ?>
			style="background-image: url('<?= postThumb(); ?>')"
		<?php endif; ?>>
	<div class="home-overlay">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-auto col-md-10 col-12 wow fadeInUpBig">
					<?php get_template_part('views/partials/repeat', 'form', [
							'title' => $fields['main_form_title'],
							'subtitle' => $fields['main_form_subtitle'],
							'text' => $fields['main_form_text'],
							'id' => '7',
					]); ?>
				</div>
			</div>
		</div>
	</div>
	<img src="<?= IMG ?>home-bottom-line.png" class="bottom-line">
</section>
<?php if ($fields['home_about_text']) : ?>
<section class="home-about-block p-100">
	<?php if($fields['home_about_img']) : ?>
		<div class="about-img-wrap">
			<img src="<?= $fields['home_about_img']['url']; ?>" alt="about-img">
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-start align-items-center">
			<div class="col-lg-6 col-md-10 col-12 d-flex flex-column align-items-start">
				<div class="base-output">
					<?= $fields['home_about_text']; ?>
				</div>
				<?php if ($fields['home_about_link']) : ?>
					<a href="<?= $fields['home_about_link']['url'];?>" class="base-link">
						<?= (isset($fields['home_about_link']['title']) && $fields['home_about_link']['title'])
								? $fields['home_about_link']['title'] : 'עוד אודותינו';
						?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php endif;
if ($fields['home_services'] || $fields['home_services_text']) : ?>
	<section class="block-output services-output">
		<div class="container">
			<?php if ($fields['home_services_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output text-center">
							<?= $fields['home_services_text']; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php if ($fields['home_services']) { foreach ($fields['home_services'] as $i => $service) {
					get_template_part('views/partials/card', 'post', [
							'post' => $service,
					]);
				}} ?>
			</div>
			<?php if ($fields['home_services_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['home_services_link']['url'];?>" class="block-link">
							<?= (isset($fields['home_services_link']['title']) && $fields['home_services_link']['title'])
									? $fields['home_services_link']['title'] : 'לכל השירותים שלנו';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<section class="form-and-benefits p-100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-12">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits'); ?>
</section>
<div class="projects-output">
	<?php if ($projects = $fields['home_projects']) : foreach ($projects as $key => $project) :
		get_template_part('views/partials/card', 'project_home', [
				'project' => $project,
		]);
	endforeach; endif;
	if ($fields['home_projects_link']) : ?>
		<div class="project-card">
			<div class="project-img project-link" <?php if ($fields['home_projects_link_back']) : ?>
				style="background-image: url('<?= $fields['home_projects_link_back']['url']; ?>')"
			<?php endif; ?>>
				<div class="project-card-overlay project-link-overlay">
					<a href="<?= $fields['home_projects_link']['url'];?>" class="base-link bigger-link">
						<?= (isset($fields['home_projects_link']['title']) && $fields['home_projects_link']['title'])
								? $fields['home_projects_link']['title'] : 'לכל הפרוייקטים';
						?>
					</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php if ($fields['home_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['home_slider_img'],
					'content' => $fields['home_slider_seo'],
			]);
}
get_template_part('views/partials/repeat', 'process');
if ($fields['home_posts'] || $fields['home_posts_text']) : ?>
	<section class="block-output posts-output">
		<div class="container">
			<?php if ($fields['home_posts_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output text-center">
							<?= $fields['home_posts_text']; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php if ($fields['home_posts']) { foreach ($fields['home_posts'] as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				}} ?>
			</div>
			<?php if ($fields['home_posts_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['home_posts_link']['url'];?>" class="block-link">
							<?= (isset($fields['home_posts_link']['title']) && $fields['home_posts_link']['title'])
									? $fields['home_posts_link']['title'] : 'לכל המאמרים שלנו';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($slider = $fields['single_slider_seo']) : ?>
<div class="reverse-slider">
	<?php get_template_part('views/partials/content', 'slider',[
			'img' => $fields['slider_img'],
			'content' => $slider,
	]);?>
</div>
<?php endif;
get_footer(); ?>
