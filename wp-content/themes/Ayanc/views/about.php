<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body process-page-back" id="members-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-10 col-12">
				<div class="base-output text-center">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['about_team_text']) : ?>
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-10 col-12">
					<div class="base-output text-center">
						<?= $fields['about_team_text']; ?>
					</div>
				</div>
			</div>
		<?php endif;
		if ($fields['team_member']) : $published_posts = count($fields['team_member'])?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['team_member'] as $x => $member) : ?>
					<div class="col-lg-4 col-md-10 col-12 mb-4 max-padding col-post card-member-col <?= ($x >= 2) ? 'show-col' : ''; ?>">
						<div class="post-card">
							<div class="post-card-column">
								<div class="post-img member-img"
										<?php if ($member['member_img']) : ?>
											style="background-image: url('<?= $member['member_img']['url']; ?>')"
										<?php endif; ?>>
								</div>
								<div class="post-card-content">
									<h3 class="post-card-title"><?= $member['member_name']; ?></h3>
									<p class="base-text text-center mb-3">
										<?= $member['member_text'];  ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<?php if ($published_posts > 3) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link" id="load-more-items-members">
						<?= 'טען עוד אנשי צוות'; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<section class="form-and-benefits p-100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-12">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits'); ?>
</section>
<?php
get_template_part('views/partials/repeat', 'process');
if ($slider = $fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider',[
				'img' => $fields['slider_img'],
				'content' => $slider,
		]);?>
	</div>
<?php endif;
get_footer(); ?>
