<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$tel_more = opt('tel_more');
$map = opt('map_image');
?>
<article class="contact-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-12">
				<div class="base-output text-center">
					<h1 class="text-center"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-xl-10 col-12 mb-5">
				<div class="form-wrapper">
					<div class="row justify-content-center align-items-stretch">
						<div class="col-lg-6 col-md-10 col-11 contact-col">
							<?php if ($tel) : ?>
								<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png">
									</div>
									<div class="contact-info">
										<h3 class="contact-type"><?= $tel; ?></h3>
									</div>
								</a>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>pop-trigger.png">
									</div>
									<div class="contact-info">
										<h3 class="contact-type"><?= $mail; ?></h3>
									</div>
								</a>
							<?php endif;
							if ($address) : ?>
								<a href="https://waze.com/ul?q=<?= $address; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png">
									</div>
									<div class="contact-info">
										<h3 class="contact-type"><?= $address; ?></h3>
									</div>
								</a>
							<?php endif; ?>
						</div>
						<div class="col-lg-6 col-12">
							<div class="form-wrapper-inside">
								<?php if ($fields['contact_form_title']) : ?>
									<h2 class="base-title text-center mb-1"><?= $fields['contact_form_title']; ?></h2>
								<?php endif;
								if ($fields['contact_form_text']) : ?>
									<p class="form-text"><?= $fields['contact_form_text']; ?></p>
								<?php endif;
								getForm('11'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
