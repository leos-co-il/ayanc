<?php
/*
Template Name: פרויקטים
*/

get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 4,
	'post_type' => 'project',
]);
$published_posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'project',
]);
?>

<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="projects-output put-here-posts">
				<?php foreach ($posts->posts as $i => $post) {
					get_template_part('views/partials/card', 'project', [
						'project' => $post,
						]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts > 4) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="project">
						<?= 'טען עוד פרוייקטים'; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="form-and-benefits p-100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-12">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits'); ?>
</section>
<?php if ($slider = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $slider,
		]);
}
get_footer(); ?>
