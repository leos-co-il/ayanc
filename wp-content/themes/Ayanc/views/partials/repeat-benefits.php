<?php if ($benefits = opt('why_us_item')) : ?>
	<div class="why-us-block">
		<div class="container pt-5">
			<?php if ($home_why_title = opt('why_us_title')) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mb-4">
						<h2 class="base-title text-center"><?= $home_why_title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center ">
				<div class="col-auto">
					<div class="row justify-content-center align-items-start">
						<?php foreach ($benefits as $key => $benefit) : ?>
							<div class="col-4 benefit-col mb-3">
								<div class="benefit-item wow zoomInUp" data-wow-delay="0.<?= $key * 2; ?>s">
									<div class="benefit-icon-wrap">
										<div class="pop-trigger-inside">
										<?php if ($benefit_icon = $benefit['why_icon']) : ?>
											<img src="<?= $benefit_icon['url']; ?>">
										<?php endif; ?>
										</div>
									</div>
									<h3 class="form-text">
										<?= $benefit['why_title']; ?>
									</h3>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
