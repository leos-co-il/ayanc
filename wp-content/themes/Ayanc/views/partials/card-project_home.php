<?php if (isset($args['project']) && $args['project']) :$link = get_the_permalink($args['project']); ?>
	<div class="project-card">
		<div class="project-img" <?php if (has_post_thumbnail($args['project'])) : ?>
			style="background-image: url('<?= postThumb($args['project']); ?>')"
		<?php endif; ?>>
			<a class="project-card-overlay" href="<?= $link; ?>">
				<h3 class="project-card-title"><?= $args['project']->post_title; ?></h3>
				<p class="project-card-text"><?= text_preview($args['project']->post_content, 8); ?></p>
			</a>
		</div>
	</div>
<?php endif; ?>
