<section class="repeat-process-block mb-5" <?php if ($back = opt('way_img_back')) : ?>
	style="background-image: url('<?= $back['url']; ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<?php if ($title = opt('way_title')) : ?>
					<h2 class="form-subtitle"><?= $title; ?></h2>
				<?php endif;
				if ($subtitle = opt('way_subtitle')) : ?>
					<h2 class="way-subtitle"><?= $subtitle; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($link = opt('way_link')) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<a href="<?= $link['url'];?>" class="base-link bigger-link">
						<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'הכירו את השלבים';
						?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
