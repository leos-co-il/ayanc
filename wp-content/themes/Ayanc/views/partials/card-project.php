<?php if (isset($args['project']) && $args['project']) :$link = get_the_permalink($args['project']); ?>
	<div class="project-card more-card basic-project-card" data-id="<?= $args['project']->ID; ?>">
		<div class="project-img" <?php if (has_post_thumbnail($args['project'])) : ?>
			style="background-image: url('<?= postThumb($args['project']); ?>')"
		<?php endif; ?>>
			<div class="project-card-overlay">
				<h3 class="project-card-title"><?= $args['project']->post_title; ?></h3>
				<p class="project-card-text"><?= text_preview($args['project']->post_content, 15); ?></p>
				<a href="<?= $link; ?>" class="base-link post-link">
					לפרוייקט
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
