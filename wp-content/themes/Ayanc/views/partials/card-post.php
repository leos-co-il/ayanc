<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']);
$type = get_post_type($args['post']->ID); ?>
	<div class="col-lg-4 col-md-10 col-12 mb-4 max-padding col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<div class="post-card-column">
				<a class="post-img <?= ($type === 'post') ? 'post-small-img' : ''; ?>" href="<?= $link; ?>"
					<?php if (has_post_thumbnail($args['post'])) : ?>
						style="background-image: url('<?= postThumb($args['post']); ?>')"
					<?php endif; ?>>
				</a>
				<div class="post-card-content">
					<h3 class="post-card-title"><?= $args['post']->post_title; ?></h3>
					<p class="base-text text-center mb-3">
						<?= text_preview($args['post']->post_content, 15); ?>
					</p>
					<a class="base-link post-link" href="<?= $link; ?>">
						<?= ($type === 'service') ? 'לשירות' : 'קרא עוד';?>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
