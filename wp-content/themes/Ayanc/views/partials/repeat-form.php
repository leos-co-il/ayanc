<?php
$f_title_base = opt('base_form_title');
$f_subtitle_base = opt('base_form_subtitle');
$f_text_base = opt('base_form_text');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '8';
?>
<div class="form-wrapper">
	<div class="form-wrapper-inside">
		<span class="close-form">
			<img src="<?= ICONS ?>close.png">
		</span>
		<h2 class="form-title"><?= $f_title_base; ?></h2>
		<h3 class="form-subtitle"><?= $f_subtitle_base; ?></h3>
		<p class="form-text"><?= $f_text_base; ?></p>
		<?php getForm($id); ?>
	</div>
</div>
