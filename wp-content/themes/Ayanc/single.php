<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = 'category';
$quantity = ($currentType === 'project') ? 2 : 3;
switch ($currentType) {
	case 'project':
		$taxname = 'project_cat';
		$sameTitle = '<h2>פרויקטים נוספים</h2>';
		break;
	case 'service':
		$taxname = 'service_cat';
		$sameTitle = '<h2>שירותים נוספים</h2>';
		break;
	case 'post':
		$taxname = 'category';
		$sameTitle = '<h2>מאמרים נוספים</h2>';
		break;
	default:
		$taxname = 'category';
		$sameTitle = '<h2>מאמרים נוספים</h2>';
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => $quantity,
	'post_type' => $currentType,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $taxname,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => $quantity,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
$post_gallery = $fields['post_gallery'];
?>
<article class="article-page-body page-body pb-5">
	<div class="container">
		<div class="row justify-content-between">
			<div class="<?= $post_gallery ? 'col-xl-5 col-lg-6' : 'col-lg-12'?> col-12 d-flex flex-column align-items-start">
				<div class="base-output base-post-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<?php if ($fields['post_file']) : ?>
					<a class="link-post-save mb-3" download href="<?= $fields['post_file']['url']; ?>">
						<img src="<?= ICONS ?>pdf.png">
						<span><?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ? $fields['post_file']['title'] : 'הורידו את הקובץ'?></span>
					</a>
				<?php endif;
				if ($more_text = $fields['post_more_content']) : ?>
					<div class="base-output">
						<?= $more_text; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 col-12 order-first-point arrows-slider">
				<?php if ($post_gallery || has_post_thumbnail()) : ?>
					<div class="base-slider gallery-slider" dir="rtl">
						<?php if (has_post_thumbnail()) : ?>
							<div class="slider-wrapper">
								<a class="slider-picture" href="<?= postThumb(); ?>" data-lightbox="images"
								   style="background-image: url('<?= postThumb(); ?>')">
								</a>
							</div>
						<?php endif;
						if ($post_gallery) : foreach ($post_gallery as $img) : ?>
							<div class="slider-wrapper">
								<a class="slider-picture" data-lightbox="images"
								   style="background-image: url('<?= $img['url']; ?>')"></a>
							</div>
						<?php endforeach; endif;?>
					</div>
				<?php endif; ?>
				<div class="sticky-form">
					<?php get_template_part('views/partials/repeat', 'form', [
							'title' => opt('post_form_title'),
							'subtitle' => opt('post_form_subtitle'),
							'text' => opt('post_form_text'),
							'id' => '9',
					]); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'process');
if ($samePosts && ($currentType !== 'project')) : ?>
	<section class="block-output posts-output">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-output text-center">
						<?= $fields['same_text'] ? $fields['same_text'] : $sameTitle; ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php elseif ($currentType === 'project') :?>
	<div class="projects-output m-70">
		<?php foreach ($samePosts as $key => $project) :
			get_template_part('views/partials/card', 'project', [
					'project' => $project,
			]);
		endforeach;?>
	</div>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
}
get_footer(); ?>
