<?php

the_post();
get_header();
$fields = get_fields();

?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="base-output text-center">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/content', 'slider',
	[
		'img' => $fields['slider_img'],
		'content' => $fields['single_slider_seo'],
	]);
get_footer(); ?>
