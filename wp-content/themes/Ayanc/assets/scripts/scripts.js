(function($) {
	// Load more items in about
	var sizeLi = $('#members-block .card-member-col').size();
	var x = 3;
	$('#load-more-items-members').click(function () {
		x = (x + 3 <= sizeLi) ? x + 3 : sizeLi;
		if (x === sizeLi) {
			$('#load-more-items-members').addClass('hide');
		}
		$('#members-block .card-member-col:lt('+x+')').addClass('show-col');
	});
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.number-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			// dots: true,
			// customPaging : function(slider, i) {
			// 	var thumb = $(slider.$slides[i]).data();
			// 	return '<a>'+(i+1)+'</a>';
			// },
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		// var accordion = $('#accordion');
		// accordion.on('shown.bs.collapse', function () {
		// 	var show = $( '.show' );
		// 	show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		// });
		// accordion.on('hidden.bs.collapse', function () {
		// 	var collapsed = $( '.collapse' );
		// 	collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		// });
	});

	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		var postType = $(this).data('type');
		var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				ids: ids,
				taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});

})( jQuery );
