<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
    <div class="container-fluid">
        <div class="row justify-content-between align-items-center">
            <div class="col d-flex justify-content-start">
				<a href="/" class="logo">
					<img src="<?= opt('logo')['url'] ?>" alt="">
				</a>
            </div>
            <div class="col-auto">
            <nav id="MainNav" class="h-100">
                    <div id="MobNavBtn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
                </nav>
            </div>
			<?php if ($tel = opt('tel')) : ?>
				<div class="col d-flex justify-content-end align-items-center">
					<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center">
						<span class="tel-number">
							<?= $tel; ?>
						</span>
						<img src="<?= ICONS ?>header-tel.png" alt="header-tel">
					</a>
				</div>
			<?php endif; ?>
        </div>
    </div>
</header>

<div class="pop-trigger pop-trigger-wrap">
	<span class="pop-trigger-inside">
		<img src="<?= ICONS ?>pop-trigger.png" alt="pop-trigger">
	</span>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<?php get_template_part('views/partials/repeat', 'form', [
							'title' => opt('pop_form_title'),
							'subtitle' => opt('pop_form_subtitle'),
							'text' => opt('pop_form_text'),
							'id' => '6',
					]); ?>
				</div>
			</div>
		</div>
	</div>
</section>
