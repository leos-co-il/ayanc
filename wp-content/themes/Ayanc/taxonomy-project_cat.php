<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 4,
	'post_type' => 'project',
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$published_posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'project',
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
?>

<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?= $query->name; ?></h1>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="projects-output put-here-posts">
				<?php foreach ($posts->posts as $i => $post) {
					get_template_part('views/partials/card', 'project', [
						'project' => $post,
						]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts && $published_posts > 4) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="project" data-tax="project_cat" data-term="<?= $query->term_id; ?>">
						<?= 'טען עוד פרוייקטים'; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="form-and-benefits p-100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-auto col-12">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits'); ?>
</section>
<?php if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => get_field('slider_img', $query),
			'content' => $slider,
		]);
}
get_footer(); ?>
