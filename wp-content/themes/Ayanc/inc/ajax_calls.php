<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}

add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$post_type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : 'post';
	$tax_name = (isset($_REQUEST['taxType'])) ? $_REQUEST['taxType'] : 'category';
	$quantity = ($post_type === 'project') ? 2 : 3;
//	$params_string = (isset($_REQUEST['params'])) ? $_REQUEST['params'] : '';
//	$hi = json_decode($params_string, true, 4);
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => $post_type,
		'posts_per_page' => $quantity,
		'post__not_in' => $ids,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => $tax_name,
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
//	$query_2 = new WP_Query([
//		'post_type' => $post_type,
//		'posts_per_page' => -1,
//		'post__not_in' => $ids,
//	]);
	$html = '';
	$result['html'] = '';
//	if (!$query_2->have_posts()) {
//		$result['hide'] = 'hide';
//	}
	if ($post_type === 'project') {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'project', [
				'project' => $item,
			]);
			$result['html'] .= $html;
		}
	} else {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
