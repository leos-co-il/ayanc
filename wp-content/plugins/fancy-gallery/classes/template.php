<?php Namespace WordPress\Plugin\GalleryManager;

abstract class Template {

  public static function init(){
    add_Filter('search_template', [static::class, 'changeSearchTemplate']);
  }

  static function changeSearchTemplate($template){
    global $wp_query;

    if (Query::isGallerySearch($wp_query) && $search_template = locate_Template(sprintf('search-%s.php', Post_Type::post_type_name)))
      return $search_template;
    else
      return $template;
  }

  public static function load($template_name, $vars = []){
		extract($vars);
    $template_path = locate_Template("{$template_name}.php");

    OB_Start();

    if (!empty($template_path))
      include $template_path;
		else
      include sprintf('%s/templates/%s.php', Core::$plugin_folder, $template_name);

		return OB_Get_Clean();
  }

}

Template::init();
